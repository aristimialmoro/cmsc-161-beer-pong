﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject ScoreUI;
    public GameObject playerOne;
    public GameObject playerTwo;

    public static int winnerNumber;


    public void Score()
    {
        //Debug.Log("Score!");
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        ScoreUI.SetActive(true);
    }

    public void HideScoreUI()
    {
        ScoreUI.SetActive(false);
    }

    public void DisplayWinner()
    {
        int playerOneScore = int.Parse(playerOne.GetComponent<Text>().text);
        int playerTwoScore = int.Parse(playerTwo.GetComponent<Text>().text);
        winnerNumber = 1;
        if (playerOneScore < playerTwoScore)
        {
            winnerNumber = 2;
        }
        else if(playerOneScore == playerTwoScore)
        {
            winnerNumber = 0;
        }

        SceneManager.LoadScene(2);
    }

}
