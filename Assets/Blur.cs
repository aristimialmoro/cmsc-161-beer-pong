﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class Blur : MonoBehaviour
{

    public PostProcessingProfile profile;
    public GameObject p1Score;
    public GameObject p2Score;

    private int playerNumber = 0;
    private float resetTimer = 4f;
    private bool holdingBall = true;
    // Use this for initialization
    void Start()
    {

        DepthOfFieldModel.Settings dofSettings = profile.depthOfField.settings;
        dofSettings.aperture = 10f;
        profile.depthOfField.settings = dofSettings;
    }

    // Update is called once per frame
    void Update()
    {

        DepthOfFieldModel.Settings dofSettings = profile.depthOfField.settings;
        //profile.depthOfField.settings.aperture.Equals(20);
        int p1CurrScore = int.Parse(p1Score.GetComponent<Text>().text);
        int p2CurrScore = int.Parse(p2Score.GetComponent<Text>().text);

        //change player number
        if (holdingBall)
        {
            if (Input.GetMouseButtonDown(0))
            {
                holdingBall = false;
            }

        }
        else
        {

            resetTimer -= Time.deltaTime;
            if (resetTimer <= 0)
            {

                if (playerNumber == 0)                  // PLAYER 1
                {
                    if (p1CurrScore > 0 && p1CurrScore <= 2)
                    {
                        dofSettings.aperture = 5f;
                        profile.depthOfField.settings = dofSettings;
                    }
                    else if (p1CurrScore >= 3)
                    {
                        dofSettings.aperture = 1f;
                        profile.depthOfField.settings = dofSettings;
                    }
                    else
                    {
                        dofSettings.aperture = 10f;
                        profile.depthOfField.settings = dofSettings;
                    }
                    playerNumber = 1;
                }
                else                                    // PLAYER 2
                {
                    if (p2CurrScore > 0 && p2CurrScore <= 2)
                    {
                        dofSettings.aperture = 5f;
                        profile.depthOfField.settings = dofSettings;
                    }
                    else if (p2CurrScore >= 3)
                    {
                        dofSettings.aperture = 1f;
                        profile.depthOfField.settings = dofSettings;
                    }
                    else
                    {
                        dofSettings.aperture = 10f;
                        profile.depthOfField.settings = dofSettings;
                    }
                    playerNumber = 0;
                }
                resetTimer = 5f;
                holdingBall = true;
            }
        }
    }
}
