﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cups : MonoBehaviour
{

    public GameObject score_p1; //reference to the ScoreText gameobject, set in editor
    public AudioClip cup; //reference to the basket sound
    public GameObject pongcup;
    public GameObject ball;

    public GameManager gameManager;
   
    void OnCollisionEnter() //if ball hits board
    {
        // audio.Play(); //plays the hit board sound
       // cup.GetComponent<AudioSource>().Play();
    }

    void OnTriggerEnter() //if ball hits basket collider
    {
        int currentScore = int.Parse(score_p1.GetComponent<Text>().text) + 1; //add 1 to the score
        score_p1.GetComponent<Text>().text = currentScore.ToString();
        AudioSource.PlayClipAtPoint(cup, transform.position); //play basket sound

        Destroy(pongcup);
        ball.GetComponent<Renderer>().enabled = false;

        if (currentScore == 10)
        {
            gameManager.DisplayWinner();
        }

        gameManager.Score();
        //ball.SetActive(false);
    }
}
