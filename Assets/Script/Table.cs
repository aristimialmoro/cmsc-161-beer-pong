﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Table : MonoBehaviour
{

    public GameObject sound;

    void OnCollisionEnter() //if ball hits board
    {
        // audio.Play(); //plays the hit board sound
        sound.GetComponent<AudioSource>().Play();
    }
}
