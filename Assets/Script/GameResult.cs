﻿using UnityEngine;

public class GameResult : MonoBehaviour {

    public GameObject player1UI;
    public GameObject player2UI;
    public GameObject drawUI;

    void Start()
    {
        if(GameManager.winnerNumber == 1)
        {
            player1UI.SetActive(true);
        }
        else if (GameManager.winnerNumber == 2)
        {
            player2UI.SetActive(true);
        }
        else
        {
            drawUI.SetActive(true);
        }
    }
}
