﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson; //This

public class Player : MonoBehaviour
{
    public GameObject ball;
    public GameObject playerCamera;
    public GameObject Grandma;
    public GameObject Malcom;
    public GameObject ballCount_P1;
    public GameObject ballCount_P2;

    public GameManager gameManager;

    public float resetTimer = 4f;
    public int playerNumber = 0;
    public FirstPersonController fpc;
    private float ballDistance = 1f;
    private float ballThrowingForce = 150f;
    private bool holdingBall = true;
    // Use this for initialization
    void Start()
    {
        Debug.Log("Initialized Player. Player 0");
        ball.GetComponent<Rigidbody>().useGravity = false;
        transform.position = new Vector3(6.13f, 3f, 6f);
       //fpc.m_MouseLook.m_CharacterTargetRot = Quaternion.Euler(0f, 180f, 0f);
        fpc.transform.position = Malcom.transform.position + Malcom.transform.up * 2 + Malcom.transform.forward * 0.4f;
        playerNumber = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (holdingBall)
        {
            ball.transform.position = playerCamera.transform.position + playerCamera.transform.forward * ballDistance;
            if (int.Parse(ballCount_P1.GetComponent<Text>().text) <= 0) //player 2 will be the last one to throw the ball
            {
                gameManager.DisplayWinner();
            }

            if (Input.GetMouseButtonDown(0))
            {

                Debug.Log("Player throws ball.");
                holdingBall = false;
                ball.GetComponent<Rigidbody>().useGravity = true;
                ball.GetComponent<Rigidbody>().AddForce(playerCamera.transform.forward * ballThrowingForce);
                ball.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                if (playerNumber == 0)
                {
                    int currentBallsP1 = int.Parse(ballCount_P1.GetComponent<Text>().text) - 1; //subtract 1 to the number of balls
                    ballCount_P1.GetComponent<Text>().text = currentBallsP1.ToString();

                }
                else
                {
                    int currentBallsP2 = int.Parse(ballCount_P2.GetComponent<Text>().text) - 1; //subtract 1 to the number of balls
                    ballCount_P2.GetComponent<Text>().text = currentBallsP2.ToString();
                }
            }

        }
        else
        {

            resetTimer -= Time.deltaTime;
            if (resetTimer <= 0)
            {

                if (playerNumber == 0)
                {
                    ball.GetComponent<Renderer>().enabled = true;
                    //ball.SetActive(true);
                    Debug.Log("Switch to Player 1.");
                    playerNumber = 1;
                    transform.position = new Vector3(6.13f, 3f, 6f);
                    fpc.m_MouseLook.m_CharacterTargetRot = Quaternion.Euler(0f, 180f, 0f);
                    fpc.transform.position = Malcom.transform.position + Malcom.transform.up * 2 + Malcom.transform.forward * 0.4f;
                    gameManager.HideScoreUI();
                }
                else
                {
                    ball.GetComponent<Renderer>().enabled = true;
                    //ball.SetActive(true);
                    Debug.Log("Switch to Player 0.");
                    playerNumber = 0;
                    transform.position = new Vector3(6.13f, 3f, -1.5f);
                    fpc.m_MouseLook.m_CharacterTargetRot = Quaternion.Euler(0f, 0f, 0f);
                    fpc.transform.position = Grandma.transform.position + Grandma.transform.up * 2 + Grandma.transform.forward * 0.6f;
                    gameManager.HideScoreUI();
                }
                Debug.Log("Time is reset. Ball is back.");
                resetTimer = 5f;
                holdingBall = true;
                ball.GetComponent<Rigidbody>().useGravity = false;
                ball.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
        }
    }
}
